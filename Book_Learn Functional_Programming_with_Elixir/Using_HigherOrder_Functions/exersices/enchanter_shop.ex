# In Chapter 4, Diving into Recursion, on page 59, we built a module called EnchanterShop that transforms
# mundane items into magical items for sale. Build this module again, but now apply the higher-order functions
# that you learned in this chapter.
defmodule EnchanterShop do

  @enchanter_name "Einer"

  def test_data do [
    %{title: "Longsword", price: 50, magic: false},
    %{title: "Healing Potion", price: 60, magic: true},
    %{title: "Rope", price: 10, magic: false},
    %{title: "Dragon's Spear", price: 100, magic: true},
    ]
  end

  def enchant_for_sale( [] , _fun ), do: []
  def enchant_for_sale( [head | tail] , fun ) do
    [fun.(head) | enchant_for_sale(tail, fun)]
  end

  def do_enchant( item = %{magic: state} ) when state == false do
    %{title: "#{@enchanter_name}'s #{item.title}",
      price: item.price * 3,
      magic: true}
  end
  def do_enchant( item ), do: item

end
