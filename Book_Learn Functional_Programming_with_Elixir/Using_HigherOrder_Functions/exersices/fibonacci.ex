# Create a function that generates the Fibonacci sequence up to a given quantity.
# 4 Use streams to produce it. You’ll need to take a look at the Stream.unfold/2 function.
# Tip: Try to make the recursive version first.
defmodule Fibonacci do

  def fibo (n) do
    Stream.unfold({ 0, 1 }, fn { p, u} -> { p, {u, u + p} } end )
    |> Enum.take(n)
  end

end
