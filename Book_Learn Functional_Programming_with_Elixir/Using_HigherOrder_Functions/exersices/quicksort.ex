# Implement the Quicksort algorithm.5 Tip: You can use the first item of the
# list to be the pivot, and employ the
# Enum.split_with/2 higher-order function.
defmodule QuickSort do

  def ascending([]), do: []
  def ascending([head | tail]) do
    IO.inspect tail, label: "The list is"
    { list_a , list_b } = Enum.split_with(tail, &( &1 <= head ) )
    ascending(list_a) ++ [head] ++ ascending(list_b)
  end

end
