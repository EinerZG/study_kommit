defmodule WordBuilder do
  def build(alphabet, position) do
    partial = fn at -> String.at(alphabet, at) end
    letters = Enum.map(position, partial)
    Enum.join(letters)
  end

  def build_two( alphabet, positions ) do
    letter = Enum.map(positions, &(String.at(alphabet, &1)))
    #letter = Enum.map(positions, &(String.at(alphabet, &1)))
    Enum.join(letter)
  end

end
