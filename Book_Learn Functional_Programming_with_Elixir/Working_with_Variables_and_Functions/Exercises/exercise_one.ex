# se definen las variables que determinan la compra de Sarah
number_bread_slices = 10
price_bread_slice = 10
number_milk_bottles = 3
price_milk_bottles = 2
number_cake = 1
price_cake = 15

# funciona anonima para determinar cuanto gasto Sarah
dollars_spend = fn n_b_s, p_b_s, n_m_b, p_m_b, n_c, p_c ->
  n_b_s * p_b_s + n_m_b * p_m_b + n_c * p_c
end

# mensaje que da la respuesta
IO.puts "The dollar spending is #{dollars_spend.(number_bread_slices, price_bread_slice, number_milk_bottles, price_milk_bottles, number_cake, price_cake)}"
