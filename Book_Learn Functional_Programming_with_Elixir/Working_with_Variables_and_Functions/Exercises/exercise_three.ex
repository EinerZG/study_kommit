# funcion anonima para aplicar un impuesto de 12%
apply_tax = fn price ->
  tax = 0.12
  IO.puts "Price: #{price * (1+tax)} - Tax: #{ price * tax }"
end

# se aplica impuesto de 12% a cada elemento de la lista
Enum.each [12.5, 30.99, 250.49, 18.80], apply_tax
