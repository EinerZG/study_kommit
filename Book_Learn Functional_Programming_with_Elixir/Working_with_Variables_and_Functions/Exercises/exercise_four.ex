defmodule MatchstickFactory do

  def boxes(number) do
      big = div(number, 50)
      number = rem(number, 50)
      mediun = div(number, 20)
      number = rem(number, 20)
      small = div(number, 5)
      number = rem(number, 5)
      "%{big: #{big}, mediun: #{mediun}, remaining_matchsticks: #{number}, small: #{small}" 
  end

end
