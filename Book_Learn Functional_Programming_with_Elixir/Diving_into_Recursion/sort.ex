defmodule Sort do

  import IO

  def ascending([]) do
    puts "ascending/1"
    []
  end

  def ascending([a]) do
    puts "ascending/2 #{a}"
    [a]
  end

  def ascending(list) do
    puts "ascending/3"

    half_size = div(Enum.count(list),2)
    {list_a, list_b} = Enum.split(list, half_size)

    [ head_a | tail_a ] = list_a
    [ head_b | tail_b ] = list_b

    puts "cabecera a: #{head_a}"
    puts "cabecera b: #{head_b}"

    merge( ascending(list_a),
           ascending(list_b)
          )

  end

  defp merge([], list_b) do
    [b | _] = list_b
    puts "merge/1 #{b}"
    list_b
  end
  defp merge(list_a, []) do
    [a | _] = list_a
    puts "merge/2 #{a}"
    list_a
  end
  defp merge([head_a | tail_a], list_b = [ head_b | _ ]) when head_a <= head_b do
    puts "merge/3 head_a #{head_a} head_b #{head_b}"
    IO.inspect list_b, label: "The list_b is"
    [head_a | merge(tail_a, list_b)]
  end
  defp merge(list_a = [head_a | _], [ head_b | tail_b ]) when head_a > head_b do
    puts "merge/4 head_a #{head_a} head_b #{head_b}"
    IO.inspect list_a, label: "The list_a is"
    [head_b | merge(list_a, tail_b)]
  end

end
