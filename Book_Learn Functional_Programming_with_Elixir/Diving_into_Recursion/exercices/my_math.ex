# We’ve written a lot of recursive functions, but not all of them are tail recursive.
# Write the tail-recursive versions of Sum.up_to/1 and Math.sum/1.
defmodule Math do

  # It generates a acomulado
  def up( n ), do: up_to( n , 0 )
  defp up_to( 0, acc ), do: acc
  defp up_to( n, acc ), do: up_to( n - 1 , acc + n )

  # sum number of the list
  def sum( [head | tail] ), do: sum_of( tail, head )
  defp sum_of( [], acc ), do: acc
  defp sum_of( [ head | tail ], acc ), do: sum_of( tail, acc + head )

end
