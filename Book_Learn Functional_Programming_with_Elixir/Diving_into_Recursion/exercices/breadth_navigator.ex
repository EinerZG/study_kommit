# In the section Adding Boundaries, on page 75, we added a depth restriction to limit how many directories deep our module
# should dive. Now create a BreadthNavigator module that has a breadth constraint; it will be the maximum number of sibling
# directories it can navigate.

defmodule BreadthNavigator do

  @max_depth 2
  @max_sibling 1

  def navigate(dir) do
    expanded_dir = Path.expand(dir)
    go_through([expanded_dir], 0, 0)
  end

  defp go_through([], _current_depth, _current_sibling), do: nil
  defp go_through(_dirs, current_depth, current_sibling) when current_depth > @max_depth or current_sibling > @max_sibling , do: nil
  defp go_through([content | rest], current_depth, current_sibling) do
    print_and_navigate(content, File.dir?(content), current_depth, current_sibling)
    go_through(rest, current_depth, current_sibling + 1 )
  end

  defp print_and_navigate(_dir, false, _current_depth, _current_sibling), do: nil
  defp print_and_navigate(dir, true, current_depth, current_sibling) do
    IO.puts dir
    children_dirs = File.ls!(dir)
    go_through(expand_dirs(children_dirs, dir), current_depth + 1, current_sibling)
  end

  defp expand_dirs([], _relative_to), do: []
  defp expand_dirs([dir | dirs], relative_to) do
    expanded_dir = Path.expand(dir, relative_to)
    [expanded_dir | expand_dirs(dirs, relative_to)]
  end

end
