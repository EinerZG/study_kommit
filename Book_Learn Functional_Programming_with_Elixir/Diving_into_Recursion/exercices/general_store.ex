defmodule GeneralStore do

  def test_data do [
    %{title: "Longsword", price: 50, magic: false},
    %{title: "Healing Potion", price: 60, magic: true},
    %{title: "Rope", price: 10, magic: false},
    %{title: "Dragon's Spear", price: 100, magic: true},
    ]
  end

  def filter_items( [] , _ ), do: []
  def filter_items( [item = %{magic: the_state} | tail], state ) when  the_state == state, do: [ item | filter_items(tail, state) ]
  def filter_items( [_head | tail], state ), do: filter_items(tail, state)

end

