# Write two recursive functions: one that finds the biggest element of a list and another that finds
# the smallest. You should use them like this:
defmodule MyList do

  # permite encontrar el numero mayor de una lista
  def max([head | tail]), do: do_max( tail, head )
  defp do_max([], m), do: m
  defp do_max([head | tail], m) when m < head, do: do_max( tail, head )
  defp do_max([head | tail], m) when m >= head, do: do_max( tail, m )

  # permite calcular el numero mayor de una lista
  def min([head | tail]), do: do_min(tail, head)
  defp do_min( [], m ), do: m
  defp do_min( [head | tail], m ) when m <= head, do: do_min( tail, m )
  defp do_min( [head | tail], m ) when m > head, do: do_min(tail, head)

end
