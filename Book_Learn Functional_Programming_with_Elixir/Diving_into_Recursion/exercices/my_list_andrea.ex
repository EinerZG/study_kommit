# Write two recursive functions: one that finds the biggest element of a list and another that finds
# the smallest. You should use them like this:
defmodule MyList do

  # permite encontrar el numero mayor de una lista
  def max([]), do: nil
  def max([], m), do: m
  def max([head | tail]), do: max( tail, head )
  def max([head | tail], m) when m < head, do: max( tail, head )
  def max([head | tail], m) when m >= head, do: max( tail, m )

  # permite calcular el numero mayor de una lista
  def min([head | tail]), do: do_min(tail, head)
  defp do_min( [], m ), do: m
  defp do_min( [head | tail], m ) when m <= head, do: do_min( tail, m )
  defp do_min( [head | tail], m ) when m > head, do: do_min(tail, head)

end
