defmodule TestMatch do

  import IO

  def match( list = [ a | _ ]) do
    IO.inspect list, label: "The list is"
    show(list)
  end

  def show([]), do: ""
  def show([a | b]), do: "#{a} #{show(b)}"

end
