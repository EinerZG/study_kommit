defmodule DungeonCrawl.CLI.BaseCommands do

  alias Mix.Shell.IO, as: Shell

  def display_options(options) do
    options
    # generates a tuple with index - name
    |> Enum.with_index(1)
    # show the information of the index and the options
    |> Enum.each(fn {option, index} ->
      Shell.info("#{index} - #{option}")
    end)

    options
  end

  def generate_question(options) do
    options = Enum.join(1..Enum.count(options),",")
    "Which one? [#{options}]\n"
  end

  def parse_answer(answer) do
    {option, _} = Integer.parse(answer)
    option - 1
  end

end
