
##  controls the start and end of the game
##  EinerZG
defmodule DungeonCrawl.CLI.Mail do

  alias Mix.Shell.IO, as: Shell

  def start_game do
    welcome_message()
    Shell.prompt("Press Enter to continue")
    crawl(hero_choice(), DungeonCrawl.Room.all())
  end

  # show the welcome message
  defp welcome_message do
    Shell.info("== Dungeon Crawl ===")
    Shell.info("You awake in a dungeon full of monsters.")
    Shell.info("You need to survive and find the exit.")
  end

  defp hero_choice do
    hero = DungeonCrawl.CLI.HeroChoice.start()
    %{hero | name: "You"}
  end

  def crawl(%{hit_points: 0}, _) do
    Shell.prompt("")
    Shell.cmd("clear")
    Shell.info("Unfortunately your wounds are too many to keep walking.")
    Shell.info("You fall onto the floor without strength to carry on.")
    Shell.info("Game over!")
    Shell.prompt("")
  end

  def crawl(character, rooms) do
    Shell.info("You keep moving forward to the next room.")
    Shell.prompt("Press Enter to continue")
    Shell.cmd("clear")

    # show state of the character
    Shell.info(DungeonCrawl.Character.current_stats(character))

    rooms
    |> select_room
    |> DungeonCrawl.CLI.RoomActionChoice.start
    |> trigger_action(character)
    |> handle_action_result

  end

  defp select_room(rooms) do
    numbers_rooms = Enum.count(rooms)
    random_rooms = :rand.uniform( 2 * numbers_rooms )
    room = cond do
      random_rooms == 1 -> Enum.at(rooms, 0)
      random_rooms > 1 -> Enum.at(rooms, 1)
    end
    room
  end

  defp trigger_action({room, action}, character) do
    Shell.cmd("clear")
    room.trigger.run(character, action)
  end

  defp handle_action_result({_, :exit}) do
    Shell.info("You found the exit. You won the game. Congratulations!")
  end
  defp handle_action_result({character, _}) do
    crawl(character, DungeonCrawl.Room.all())
  end

end
