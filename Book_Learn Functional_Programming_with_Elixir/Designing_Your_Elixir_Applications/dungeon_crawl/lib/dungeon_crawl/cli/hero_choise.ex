#
# Allow to select the heros
# EinerZG
#
defmodule DungeonCrawl.CLI.HeroChoice do

  alias Mix.Shell.IO, as: Shell
  import DungeonCrawl.CLI.BaseCommands

  def start do
    Shell.cmd("clear")
    Shell.info("Start by choosing your hero")

    # load all heros
    heroes = DungeonCrawl.Heroes.all()
    # the search function is generated
    find_hero_by_index = &Enum.at(heroes, &1)

    heroes
    # get the name of all heros
    |> Enum.map(&(&1.name))
    # ask by the favorite hero
    |> generate_question
    # wait for the hero selection
    |> Shell.prompt
    # get the number
    |> parse_answer
    # find the hero by number
    |> find_hero_by_index.()
    # confirm hero selection
    |> confirm_hero
  end

  defp confirm_hero(chosen_hero) do
    Shell.cmd("clear")
    Shell.info(chosen_hero.description)
    if Shell.yes?("Confirm?"), do: chosen_hero, else: start()
  end

end
