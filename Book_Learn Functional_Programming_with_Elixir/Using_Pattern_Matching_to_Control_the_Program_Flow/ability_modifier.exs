user_input = IO.gets "Write your ability score:\n"
# the integer return a match with two values
case Integer.parse(user_input) do
  :error -> IO.puts "Ivalid ability score: #{user_input}"
  {ability_score, _} ->
    ability_modifier = (ability_score - 10) / 2
    IO.puts "Your ability modifier is #{ability_modifier}"
end
