user_input = IO.gets "Write your ability score:"

result = case Integer.parse(user_input) do
  :error -> "Inval ability score: #{user_input}"
  # use guard clauses (when ability_score >= 0)
  {ability_score, _} when ability_score >= 0 ->
    ability_modifier = (ability_score - 10) / 2
    "Your ability ability_modifier is #{ability_modifier}"
  end

IO.puts result
