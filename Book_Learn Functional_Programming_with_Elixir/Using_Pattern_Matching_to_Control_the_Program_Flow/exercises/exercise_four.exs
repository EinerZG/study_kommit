# Create an Elixir script where users can type their salary and see the income
# tax and the net wage. You can use the module from the previous exercise, but
# this script should parse the user input and display a message when users type
# something that is not a valid number.

defmodule CalculateTax do

  def tax_cost(salary) do
    salary_with_tax = cond do
      salary <= 2000 -> "You salary: #{salary}, tax: 0"
      salary <= 3000 -> "You salary: #{salary * 0.95}, tax: #{salary * 0.05}"
      salary <= 6000 -> "You salary: #{salary * 0.9}, tax: #{salary * 0.1}"
      salary > 6000 -> "You salary: #{salary * 0.85}, tax #{salary * 0.15}"
    end
    salary_with_tax
  end

end

user_input = IO.gets "Write your ability salary:\n"

case Integer.parse(user_input) do
  :error -> IO.puts "Invalid salary: #{user_input}"
  {salary, _} ->
    salary_with_tax = CalculateTax.tax_cost(salary)
    IO.puts salary_with_tax
  end
