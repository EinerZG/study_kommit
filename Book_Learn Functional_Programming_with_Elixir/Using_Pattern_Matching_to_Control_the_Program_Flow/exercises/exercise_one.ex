# n RPGs, players have points to spend on their character attributes. Create a
# function that returns the total number of points players have spent on their
# characters. The function will receive a map containing the strength, dexterity,
# and intelligence values. Each point in strength should be multi- plied by two,
# and dexterity and intelligence should be multiplied by three.

defmodule PointsPlayers do
  def generate_points( abilities ) do
      %{strength: st_value = st_value, dexterity: de_value, intelligence: in_value} = abilities
      "strength: #{st_value * 2}, dexterity: #{de_value * 3}, intelligence: #{in_value * 3}"
  end
end
