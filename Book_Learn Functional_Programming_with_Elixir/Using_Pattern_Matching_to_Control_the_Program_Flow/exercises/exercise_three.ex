# Create a function that calculates income tax following these rules: a salary
# equal or below $2,000 pays no tax; below or equal to $3,000 pays 5%; below or
# equal to $6,000 pays 10%; everything higher than $6,000 pays 15%.

defmodule CalculateTax do

  def tax_cost(salary) when salary <= 2000, do: 0
  def tax_cost(salary) when salary <= 3000, do: salary * 0.05
  def tax_cost(salary) when salary <= 6000, do: salary * 0.1
  def tax_cost(salary) when salary > 6000, do: salary * 0.15

end
