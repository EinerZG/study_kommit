# Create a function that returns Tic-Tac-Toe game winners. You can repre- sent
# the board with a tuple of nine elements, where each group of three items is a
# row. The return of the function should be a tuple. When we have a winner, the
# first element should be the atom :winner, and the second should be the player.
# When we have no winner, the tuple should contain one item that is the atom :no_winner.
# It should work like this:

defmodule TicTacToe do

  def winner({z, z, z,
            _, _, _,
            _, _, _ }), do: {:winner, z}

  def winner({ _, _, _,
            z, z, z,
            _, _, _ }), do: {:winner, z}

  def winner({_, _, _,
              _, _, _,
              z, z, z}), do: {:winner, z}

  def winner({z, _, _,
              z, _, _,
              z, _, _ }), do: {:winner, z}

  def winner({_, z, _,
              _, z, _,
              _, z, _ }), do: {:winner, z}

  def winner({_, _, z,
              _, _, z,
              _, _, z }), do: {:winner, z}

  def winner({z, _, _,
              _, z, _,
              _, _, z }), do: {:winner, z}

  def winner({_, _, z,
              _, z, _,
              z, _, _ }), do: {:winner, z}

  def winner( _game ), do: {:no_winner}

end
