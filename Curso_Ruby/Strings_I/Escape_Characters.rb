
puts "Hello world"
puts 'Hello world'

puts "Juliet said 'Goodbye' to Romeo"
puts 'Juliet said "Goodbye" to Romeo'
puts "Juliet said \"Goodbye\" to Romeo"
puts 'Juliet said \'Goodbye\' to Romeo'

####################################
puts
puts

puts "Let's add line break\nright here"

puts

puts "\tInsert a tab right here"
puts "This is normal"
