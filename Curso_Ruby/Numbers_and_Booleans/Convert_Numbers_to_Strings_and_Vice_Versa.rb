# convert string to others
str = "5"
p str
p str.class

p str.to_i.class

p str.to_f
p str.to_f.class

# convet integer to others
number = 10

number.to_s
number.to_f

number.to_s.class
number.to_f.class

# convert float to others
PI = 3.141159

p PI.to_s
p PI.to_s.class
