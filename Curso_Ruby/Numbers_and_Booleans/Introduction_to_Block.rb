10.times{ puts "Einer is awesome!!!" }
3.times { |i| puts "We are in number #{i}" }

3.times do |i|
  puts "We are currenly on loop number #{i}"
  puts "Einer is incredible!"
  puts "I´m having so much fun learning Ruby!"
end

# Use the times method to output the first
# ten multiples of 3
10.times{ |i| puts i.next * 3 }
