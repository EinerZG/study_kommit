defmodule RumblWeb.User do
  use RumblWeb, :model
  # defstruct [:id, :name, :username, :password]

  schema "users" do
    field :name, :string
    field :username, :string
    field :password, :string, virtual: true # es campo momentaneo que no almacena info en la bd
    field :password_hash, :string
    has_many :videos, RumblWeb.Video

    timestamps()
  end

  def changeset(model, params \\ %{}) do
    model
    # se defininen los campos obligatorios
    |> cast(params, ~w(name username)a)
    |> validate_required([:name, :username])
    # se valida el tamano del username
    |> validate_length(:username, min: 1, max: 20)
    |> unique_constraint(:username)
  end

  def registration_changeset(model, params) do
    model
    |> changeset(params)
    |> cast(params, ~w(password)a)
    |> validate_required(:password)
    |> validate_length(:password, min: 6, max: 100)
    |> put_pass_hash()
  end

  defp put_pass_hash(changeset) do
    case changeset do
      %Ecto.Changeset{valid?: true, changes: %{password: pass}} ->
        put_change(changeset, :password_hash, Comeonin.Bcrypt.hashpwsalt(pass))
      _ ->
        changeset
    end
  end
end
