defmodule RumblWeb.Video do
  use RumblWeb, :model

  @primary_key {:id, Rumbl.Multimedia.Permalink, autogenerate: true}
  schema "videos" do
    field :description, :string
    field :title, :string
    field :url, :string
    field :slug, :string
    belongs_to :user, RumblWeb.User
    belongs_to :category, RumblWeb.Category
    timestamps()
  end

  # def changeset(video, attrs) do
  #   video
  #   |> cast(attrs, [:url, :title, :description])
  #   |> validate_required([:url, :title, :description])
  # end

  @required_fields ~w(url title description)a
  @optional_fields ~w()
  def changeset(model, params \\ %{}) do
    model
    |> cast(params, @required_fields, @optional_fields)
    |> slugify_title()
    |> assoc_constraint(:category)
  end

  defp slugify_title(changeset) do
    if title = get_change(changeset, :title) do
      new_path = slugify(title)
      put_change(changeset, :slug, new_path)
    else
      changeset
    end
  end

  defp slugify(str) do
    str
    |> String.downcase()
    |> String.replace(~r/[^\w-]+/u, "-")
  end

end

defimpl Phoenix.Param, for: RumblWeb.Video do
  def to_param(%{slug: slug, id: id}) do
    IO.puts slug
    "#{id}-#{slug}"
  end
end
