defmodule Todo.Supervisor do
  use Supervisor

  def start_link do
    # Handles two-way communication
    Supervisor.start_link(__MODULE__, nil)
  end

  def init(_) do
    #start todo database with the name folder database
    #start todo cache with the empty arguments
    processes = [
      #worker(Todo.ProcessRegistry, []),
      supervisor(Todo.Database, ["./persist/"]),
      supervisor(Todo.ServerSupervisor, []),
      #worker(Todo.Cache, [])
    ]
    # start workers with the description
    supervise(processes, strategy: :one_for_one)
  end

end
