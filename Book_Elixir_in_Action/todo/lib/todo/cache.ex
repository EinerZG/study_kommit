defmodule Todo.Cache do
  #use GenServer

  ## client
  # def start_link do
  #   IO.puts("Starting to-do cache")
  #   # __MODULE__ this approach removes this minor duplication and guards the code against a possible change of the module name
  #   GenServer.start_link(__MODULE__, nil, name: :todo_cache)
  # end

  def server_process(todo_list_name) do
    case Todo.Server.whereis(todo_list_name) do
      :undefined ->
        #GenServer.call(:todo_cache, {:server_process, todo_list_name})
        create_server(todo_list_name)
      pid ->
        pid
    end
  end

  defp create_server(todo_list_name) do
    case Todo.ServerSupervisor.start_child(todo_list_name) do
      {:ok, pid} -> pid
      {:error, {:already_started, pid}} -> pid
    end
  end

  ### calback server
  # def init(_) do
  #   {:ok, nil}
  # end

  # def handle_call({:server_process, todo_list_name}, _, todo_servers) do
  #   todo_server_pid =
  #     case Todo.Server.whereis(todo_list_name) do
  #       :undefined ->
  #         IO.puts "indeifnido ----"
  #         {:ok, pid} = Todo.ServerSupervisor.start_child(todo_list_name)
  #         pid
  #       pid ->
  #         IO.inspect pid
  #         pid
  #     end
  #   {:reply, todo_server_pid, todo_servers}
  # end
end
