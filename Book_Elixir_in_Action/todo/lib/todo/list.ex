defmodule Todo.List do
  defstruct auto_id: 1, entries: %{}

  #You can accept a keyword list as the last argument of your function, and make that argument default to an empty list:
  def new(entries \\ []) do
    Enum.reduce(
      entries,
      %Todo.List{},
      &add_entry(&2, &1)
    )
  end

  def add_entry( %Todo.List{entries: entries, auto_id: auto_id} = todo_list, entry) do
    entry = Map.put(entry, :id, auto_id)
    new_entries = Map.put(entries, auto_id, entry)
    %Todo.List{todo_list | entries: new_entries, auto_id: auto_id + 1}
  end

  def update_entry(
        %Todo.List{entries: entries} = todo_list,
        entry
      ) do
    {:ok, existing_entry} = Map.fetch(entries, entry.id)
    patched_entry = Map.merge(existing_entry, entry)
    new_entries = Map.put(entries, entry.id, patched_entry)
    %Todo.List{todo_list | entries: new_entries}
  end

  def delete_entry(
        %Todo.List{entries: entries} = todo_list,
        entry
      ) do
    new_entries = Map.delete(entries, entry.id)
    %Todo.List{todo_list | entries: new_entries}
  end

  def entries(%Todo.List{entries: entries}, date) do
    entries
    |> Stream.filter(fn {_, entry} ->
      entry.date == date
    end)
    |> Enum.map(fn {_, entry} ->
      entry
    end)
  end
end
