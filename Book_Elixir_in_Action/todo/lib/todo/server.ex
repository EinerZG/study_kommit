defmodule Todo.Server do
  use GenServer

  #### External API
  def start_link(name) do
    IO.puts("Starting to-do server for #{name}.")
    GenServer.start_link(Todo.Server, name, name: {:global,{:todo_server, name}})
  end

  #defp via_tuple(name) do
  #  {:via, Todo.ProcessRegistry, {:todo_server, name}}
  #end

  defp via_tuple(worker_id) do
    {:via, :gproc, {:n, :l, {:todo_server, worker_id}}}
  end

  def whereis(name) do
    #Todo.ProcessRegistry.whereis_name({:todo_server, name})
    #:gproc.whereis_name({:n, :l, {:todo_server, name}})
    :global.whereis_name({:todo_server, name})
  end

  def entries(todo_server, date) do
    GenServer.call(todo_server, {:entries, date})
  end

  def add_entry(todo_server, new_entry) do
    GenServer.cast(todo_server, {:add_entry, new_entry})
  end

  def update_entry(todo_server, entry) do
    GenServer.cast(todo_server, {:update_entry, entry})
  end

  def delete_entry(todo_server, entry) do
    GenServer.cast(todo_server, {:delete_entry, entry})
  end

  #### GenServer API
  def init(name) do
    {:ok, {name, Todo.Database.get(name) || Todo.List.new()}}
  end

  def handle_cast({:add_entry, new_entry}, {name, todo_list}) do
    new_state = Todo.List.add_entry(todo_list, new_entry)
    Todo.Database.store(name, new_state)
    {:noreply, {name, new_state}}
  end

  # def handle_cast({:add_entry, new_entry}, todo_list) do
  #  new_state = Todo.List.add_entry(todo_list, new_entry)
  #  {:noreply, new_state}
  # end

  # def handle_cast({:update_entry, entry}, todo_list) do
  #  {:noreply, Todo.List.update_entry(todo_list, entry)}
  # end

  # def handle_cast({:delete_entry, entry}, todo_list) do
  #  {:noreply, Todo.List.delete_entry(todo_list, entry)}
  # end

  def handle_call({:entries, date}, _, {name, todo_list}) do
    {:reply, Todo.List.entries(todo_list, date), {name, todo_list}}
  end
end
