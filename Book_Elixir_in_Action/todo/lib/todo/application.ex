defmodule Todo.Application do
  use Application

  def start(_, _) do
    response = Todo.Supervisor.start_link
    # starts the http server
    Todo.Web.start_server
    response
  end

end
