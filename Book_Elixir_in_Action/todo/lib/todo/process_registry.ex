defmodule Todo.ProcessRegistry do
  use GenServer
  # kernel only with send/2
  import Kernel, except: [send: 2]

  def start_link do
    IO.puts("Starting process registry")
    GenServer.start_link(__MODULE__, nil, name: :process_registry)
  end

  def register_name(key, pid) do
    GenServer.call(:process_registry, {:register_name, key, pid})
  end

  def whereis_name(key) do
    GenServer.call(:process_registry, {:whereis_name, key})
  end

  def unregister_name(key) do
    GenServer.call(:process_registry, {:unregister_name, key})
  end

  def send(key, message) do
    case whereis_name(key) do
      :undefined ->
        {:badarg, {key, message}}

      pid ->
        # send info to
        Kernel.send(pid, message)
        pid
    end
  end

  ## server

  def init(_) do
    :ets.new(:table_process_registry, [:set, :named_table, :protected])
    # without state
    {:ok, nil}
  end

  def handle_call({:register_name, key, pid}, _, state) do
    case read_cached(key) do
      :undefined ->
        # monitor registred process
        Process.monitor(pid)
        # store the alias-to-pig mapping
        cache_response(key, pid)
        {:reply, :yes, state}

      _ ->
        {:reply, :no, state}
    end
  end

  defp read_cached(key) do
    case :ets.lookup(:table_process_registry, key) do
      # there is a row
      [{^key, cached}] -> cached
      # nothing is foung
      _ -> :undefined
    end
  end

  defp cache_response(key, pid) do
    :ets.insert(:table_process_registry, {key, pid})
    pid
  end

  def handle_call({:whereis_name, key}, _, process_resgistry) do
    {
      :reply,
      read_cached(key),
      process_resgistry
    }
  end

  def handle_call({:unregister_name, key}, _, state) do
    :ets.delete(:table_process_registry, key)
    {:reply, key, state}
  end

  def handle_info({:DOWN, _, :process, pid, _}, state) do
    :ets.match_delete(:table_process_registry, pid)
    {:noreply, state}
  end

  def handle_info(_, state), do: {:noreply, state}

  defp deregister_pid(process_registry, pid) do
    # We'll walk through each {key, value} item, and keep those elements whose
    # value is different to the provided pid.
    process_registry
    |> Enum.filter(fn {_registered_alias, registered_process} -> registered_process != pid end)
    |> Enum.into(%{})
  end
end
