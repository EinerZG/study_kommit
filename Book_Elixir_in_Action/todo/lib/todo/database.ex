defmodule Todo.Database do
  @pool_size 3

  ## Client
  def start_link(db_folder) do
    # GenServer.start(__MODULE__, db_folder, name: :database_server)
    Todo.PoolSupervisor.start_link(db_folder, @pool_size)
  end

  def store(key, data) do
    {_results, bad_nodes} =
      :rpc.multicall(
        __MODULE__, :store_local, [key, data],
        :timer.seconds(5)
      )
    Enum.each(bad_nodes, &IO.puts("Store failed on node #{&1}"))
    :ok
  end

  def store_local(key, data) do
    key
    |> choose_worker
    |> Todo.DatabaseWorker.store(key, data)
  end

  def get(key) do
    key
    |> choose_worker
    |> Todo.DatabaseWorker.get(key)
  end

  defp choose_worker(key) do
    # increments range to 1..3
    :erlang.phash2(key, @pool_size) + 1
  end

  ## Server
  def init(db_folder) do
    File.mkdir_p(db_folder)
    {:ok, start_workers(db_folder)}
  end

  # def handle_call({:choose_worker, key}, _, workers) do
  #  worker_key = :erlang.phash(key, 3)
  #  {:reply, Map.get(workers, worker_key), workers}
  # end

  # the three workers are generate
  defp start_workers(db_folder) do
    for index <- 1..3, into: Map.new() do
      IO.puts("workrs #{index}")
      {:ok, pid} = Todo.DatabaseWorker.start_link(db_folder)
      {index - 1, pid}
    end
  end
end
