defmodule Todo.MixProject do
  use Mix.Project

  def project do
    [
      app: :todo,
      version: "0.1.0",
      elixir: "~> 1.7",
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      #extra_applications: [:logger],
      applications: [:gproc, :cowboy, :plug],
      mod: {Todo.Application, []},
      # provides a default applicaction environment
      env: [
        port: 5454
      ]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:gproc, "0.3.1"},
      {:cowboy, "1.0.4"},
      {:plug, "1.3.0"},
      {:exrm, "1.0.8"},
      #{:httpoison, "0.8.0", only: :test}
    ]
  end
end
