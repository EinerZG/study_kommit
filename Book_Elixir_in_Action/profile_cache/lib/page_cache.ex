defmodule PageCache do
  use GenServer

  def start_link do
    GenServer.start_link(__MODULE__, nil, name: :page_cache)
  end

  def cache(key, lambda) do
    GenServer.call(:page_cache, {:cache, key, lambda})
  end

  # server
  def init(_) do
    {:ok, %{}}
  end

  def handle_call({:cache, key, lambda}, _, cache) do
    case Map.get(cache, key) do
      nil ->
        response = lambda.()
        {:reply, response, Map.put(cache, key, response)}
      # if is firts always is mach true
      response ->
        {:reply, response, cache}
    end
  end
end
