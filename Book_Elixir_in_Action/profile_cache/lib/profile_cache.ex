defmodule ProfileCache do
  @moduledoc """
  Documentation for ProfileCache.
  """

  @doc """
  Hello world.

  ## Examples

      iex> ProfileCache.hello()
      :world

  """
  def hello do
    :world
  end
end
