defmodule Profiler do

  def run(module, times) do
    module.start_link
    time = measure(module, times)
    time = time / 1000_000
    times_per_second = times / time
    "time #{time}, times per sec: #{times_per_second}"
  end

  defp measure(module, times) do
    {time, _} = :timer.tc(fn ->
      for _ <- 1..times do
        module.cache(:index, &WebServer.index/0)
      end
    end)
    time
  end

end
