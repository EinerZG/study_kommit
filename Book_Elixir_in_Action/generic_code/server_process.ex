defmodule ServerProcess do

  def start(callback_module) do
    spawn(fn ->
      inicial_state = callback_module.init
      loop(callback_module, inicial_state)
    end)
  end

  defp loop(callback_module, current_state) do
    receive do
      {:call, request, caller} ->
        {response, new_state} = callback_module.handle_call(request, current_state)
        IO.puts("entre loop call")
        send(caller, {:response, response})
        loop(callback_module, new_state)
      {:cast, request} ->
        new_state = callback_module.handle_cast(request,current_state)
        IO.puts("entre loop cast")
        loop(callback_module, new_state)
    end
  end

  # send de infomation
  def cast(server_pid, request) do
    send(server_pid, {:cast, request})
  end

  def call(server_pid, request) do
    send(server_pid, {:call, request, self})
    # get the data send to the loop
    receive do
      {:response, response} ->
        IO.puts("entre abajo")
        response
    end
  end
end

defmodule KeyValueStore do

  def init do
    HashDict.new()
  end

  def start do
    ServerProcess.start(KeyValueStore)
  end

  def put(pid, key, value) do
    ServerProcess.cast(pid, {:put, key, value})
  end

  def get(pid, key) do
    ServerProcess.call(pid, {:get, key})
  end

  def handle_call({:put, key, value}, state) do
    {:ok, HashDict.put(state, key, value)}
  end

  def handle_call({:get, key}, state) do
    {HashDict.get(state, key), state}
  end

  def handle_cast({:put, key, value}, state) do
    HashDict.put(state, key, value)
  end

end
